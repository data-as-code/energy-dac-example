import base64
import io

import pandas as pd
import requests


def load() -> pd.DataFrame:
    result = requests.get(
        "https://gitlab.com/api/v4/projects/43746775/repository/files/v2.0.1%2Fenergy-v2.0.1.parquet?ref=ab7225ad0b07fe8ede4240ae89a82f7bf84abde9",
    )
    decoded_data = base64.b64decode(result.json()["content"])
    io_data = io.BytesIO(decoded_data)
    return pd.read_parquet(io_data)
