# DaC example: Energy

This GitLab project is used to host the code to generate an example of a [DaC](https://github.com/data-as-code/dac) package, 
containing energy data coming from [here](https://www.kaggle.com/datasets/gpreda/energy-balance-in-europe).

It contains 7 versions, that have been imagined to be created in this way:

* `1.0.0`: first release, gross available energy in most countries in europe from 1990 until 2009
* `1.1.0`: add rows, now data are until 2019
* `2.0.0`: change schema model, now values can either be gross available energy or energy consumption in various sectors
* Spotted an issue in the data creation that introduced many `NaN` values. Patch release to support all minors:
  * `1.0.1`
  * `1.1.1`
  * `2.0.1`
* `2.0.2` data are no more built in the package, but retrieved via internet


## Quickstart

```sh
python -m pip install dac-example-energy --index-url https://gitlab.com/api/v4/projects/43746775/packages/pypi/simple
```
to download the latest version (`2.0.2`), or
```sh
python -m pip install dac-example-energy==VERSION --index-url https://gitlab.com/api/v4/projects/43746775/packages/pypi/simple
```
if you want to choose a specific `VERSION`.

After installing it, you can

* load the data as pandas DataFrame
  ```python
  >>> from dac_example_energy import load
  >>> load()
                           nrg_bal_name            siec_name geo  TIME_PERIOD  OBS_VALUE
  0      Final consumption - energy use   Solid fossil fuels  AL         1990   6644.088
  1      Final consumption - energy use   Solid fossil fuels  AL         1991   3816.945
  2      Final consumption - energy use   Solid fossil fuels  AL         1992   1067.475
  3      Final consumption - energy use   Solid fossil fuels  AL         1993    525.540
  4      Final consumption - energy use   Solid fossil fuels  AL         1994    459.514
  ...                               ...                  ...  ..          ...        ...
  71155          Gross available energy  Non-renewable waste  XK         2015      0.000
  71156          Gross available energy  Non-renewable waste  XK         2016      0.000
  71157          Gross available energy  Non-renewable waste  XK         2017      0.000
  71158          Gross available energy  Non-renewable waste  XK         2018      0.000
  71159          Gross available energy  Non-renewable waste  XK         2019      0.000
  
  [71160 rows x 5 columns]
  ```
* get the columns defined in the Schema:
  ```python
  >>> from dac_example_energy import Schema as EnergySchema
  >>> help(EnergySchema)
  class Schema(pandera.model.SchemaModel)
   |  Schema(*args, **kwargs) -> pandera.typing.common.DataFrameBase[~TSchemaModel]
   |
  ...
   |
   |  location = 'geo'
   |
   |  source = 'siec_name'
   |
   |  value_in_gwh = 'OBS_VALUE'
   |
   |  value_meaning = 'nrg_bal_name'
   |
   |  year = 'TIME_PERIOD'
   ...
   ```
* reference the DataFrame column names via the schema alias (e.g. `EnergySchema.year` instead of `"TIME_PERIOD"`)
  ```python
  >>> from dac_example_energy import load, Schema as EnergySchema
  >>> load()[EnergySchema.year].min(), load()[EnergySchema.year].max()
  (1990, 2019)
  ```
* first `python -m pip install pandera[io]`, then export the schema to YAML:
  ```python
  >>> from dac_example_energy import Schema as EnergySchema
  >>> print(EnergySchema.to_yaml())
  schema_type: dataframe
  version: 0.13.4
  columns:
    siec_name:
      title: null
      description: Source of energy
      dtype: str
      nullable: false
      checks:
        isin:
        - Solid fossil fuels
        - Manufactured gases
        - Electricity
        - Natural gas
        - Heat
        - Nuclear heat
        - Oil and petroleum products (excluding biofuel portion)
        - Peat and peat products
        - Renewables and biofuels
        - Oil shale and oil sands
        - Total
        - Non-renewable waste
      unique: false
      coerce: false
      required: true
      regex: false
    nrg_bal_name:
      title: null
      description: Meaning of the value
      dtype: str
      nullable: false
  ...
  ```
* First `python -m pip install pandera[strategies]`, then generate random sample of data fitting the schema:
  ```python
  >>> from dac_example_energy import Schema as EnergySchema
  >>> EnergySchema.example()
                  siec_name                                    nrg_bal_name geo  TIME_PERIOD     OBS_VALUE
  0      Manufactured gases  Final consumption - other sectors - energy use  NO         1996 -3.333333e-01
  1  Peat and peat products                          Gross available energy  AT         1992 -4.066002e+16
  ```
  and use it to test your code, following the [hypothesis way to test](https://pandera.readthedocs.io/en/stable/data_synthesis_strategies.html)
  ```
  >>> from dac_example_energy import Schema as EnergySchema
  >>>
  >>> def f(df):
  >>>     return df[EnergySchema.year].min()  # we are not dealing with the case of an empty dataframe
  >>>
  >>> @hypothesis.given(EnergySchema.strategy())
  >>> def test_f(df):
  >>>     assert f(df) >= 1990
  >>>
  >>> test_f()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    File "<stdin>", line 2, in test_f
    File "/Users/cesco/Desktop/foo/venv/lib/python3.11/site-packages/hypothesis/core.py", line 1396, in wrapped_test
      raise the_error_hypothesis_found
    File "<stdin>", line 3, in test_f
  AssertionError
  Falsifying example: test_f(
      df=
          Empty DataFrame
          Columns: [siec_name, nrg_bal_name, geo, TIME_PERIOD, OBS_VALUE]
          Index: []
      ,
  )
  ```
