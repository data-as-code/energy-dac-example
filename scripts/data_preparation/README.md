# Prepare parquet data for dac

[Data source](https://www.kaggle.com/datasets/gpreda/energy-balance-in-europe)

Convert the .xls files into csv and then run the `prepare.py` script. This will produce several parquet files for various dac releases.
