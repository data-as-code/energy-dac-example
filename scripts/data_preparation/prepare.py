# %%

import numpy as np
import pandas as pd

# %%

energy_balance = pd.read_csv("energy_balance_eu.csv")
print(energy_balance.shape)
energy_balance.head(3)

# %%

energy_balance["unit"].unique()

# %%

nrg_bal_dict = pd.read_csv("nrg_bal_dict.csv")
nrg_bal_dict.head(1)

# %%

siec_dict = pd.read_csv("siec_dict.csv")
siec_dict.head(1)

# %%

readable_energy_balance = (
    energy_balance.merge(nrg_bal_dict, on="nrg_bal", how="inner")
    .merge(siec_dict, on="siec", how="inner")
    .drop(columns=["nrg_bal", "siec"])  # unit is always GWh
    .loc[:, ["nrg_bal_name", "siec_name", "unit", "geo", "TIME_PERIOD", "OBS_VALUE"]]
)
print(readable_energy_balance.shape)
readable_energy_balance.head(1)

# %%

for col in ["nrg_bal_name", "siec_name", "unit", "geo"]:
    print(col, readable_energy_balance[col].unique())
    print()

# %% [markdown]
# # Release timeline
# * 1.0.0 (first release, with data until 2009, and some NaN)
# * 1.1.0 (add data until 2019)
# * 2.0.0 (add more energy fields)
# * 1.0.1, 1.1.1, 2.0.1 (fx NaN values)

# %% [markdown]
# ## 1.0.0
# %%

df_1_0_0 = readable_energy_balance[
    (readable_energy_balance["nrg_bal_name"].isin(["Gross available energy"]))
    & (readable_energy_balance["unit"] == "GWH")
    & (readable_energy_balance["TIME_PERIOD"] < 2010)
].drop(columns=["nrg_bal_name", "unit"])
df_1_0_0["OBS_VALUE"].where(cond=np.random.rand(len(df_1_0_0)) > 0.2, inplace=True)
df_1_0_0.to_parquet("energy-v1.0.0.parquet", index=False)
df_1_0_0.head(1)
# %% [markdown]
# ## 1.0.1
# %%

df_1_0_1 = readable_energy_balance[
    (readable_energy_balance["nrg_bal_name"].isin(["Gross available energy"]))
    & (readable_energy_balance["unit"] == "GWH")
    & (readable_energy_balance["TIME_PERIOD"] < 2010)
].drop(columns=["nrg_bal_name", "unit"])
df_1_0_1["OBS_VALUE"].where(cond=np.random.rand(len(df_1_0_1)) > 0.2, inplace=True)
df_1_0_1.to_parquet("energy-v1.0.1.parquet", index=False)
df_1_0_1.head(1)

# %% [markdown]
# ## 1.1.0
# %%

df_1_1_0 = readable_energy_balance[
    (readable_energy_balance["nrg_bal_name"].isin(["Gross available energy"]))
    & (readable_energy_balance["unit"] == "GWH")
].drop(columns=["nrg_bal_name", "unit"])
df_1_1_0["OBS_VALUE"].where(cond=np.random.rand(len(df_1_1_0)) > 0.2, inplace=True)
df_1_1_0.to_parquet("energy-v1.1.0.parquet", index=False)
df_1_1_0.head(1)

# %% [markdown]
# ## 1.1.1
# %%

df_1_1_1 = readable_energy_balance[
    (readable_energy_balance["nrg_bal_name"].isin(["Gross available energy"]))
    & (readable_energy_balance["unit"] == "GWH")
].drop(columns=["nrg_bal_name", "unit"])
df_1_1_1["OBS_VALUE"].where(cond=np.random.rand(len(df_1_1_1)) > 0.2, inplace=True)
df_1_1_1.to_parquet("energy-v1.1.1.parquet", index=False)
df_1_1_1.head(1)

# %% [markdown]
# ## 2.0.0
# %%

df_2_0_0 = readable_energy_balance[
    (
        readable_energy_balance["nrg_bal_name"].isin(
            [
                "Gross available energy",
                "Final consumption - energy use",
                "Final consumption - non-energy use",
                "Final consumption - other sectors - energy use",
                "Final consumption - transport sector - energy use",
            ]
        )
    )
    & (readable_energy_balance["unit"] == "GWH")
].drop(columns=["unit"])
df_2_0_0["OBS_VALUE"].where(cond=np.random.rand(len(df_2_0_0)) > 0.2, inplace=True)
df_2_0_0.to_parquet("energy-v2.0.0.parquet", index=False)
df_2_0_0.head(1)

# %% [markdown]
# ## 2.0.1
# %%

df_2_0_1 = readable_energy_balance[
    (
        readable_energy_balance["nrg_bal_name"].isin(
            [
                "Gross available energy",
                "Final consumption - energy use",
                "Final consumption - non-energy use",
                "Final consumption - other sectors - energy use",
                "Final consumption - transport sector - energy use",
            ]
        )
    )
    & (readable_energy_balance["unit"] == "GWH")
].drop(columns=["unit"])
df_2_0_1.to_parquet("energy-v2.0.1.parquet", index=False)
df_2_0_1.head(1)

# %%
