import requests
import typer
from rich import print


def delete_package(gitlab_token: str, package_name: str, token_type: str = "JOB"):
    """
    Use token_type="PRIVATE" for a private token (e.g. developer running on his own machine).
    """
    print(
        f"Plan to delete packages with name [bold red]{package_name}[/bold red] from the GitLab registry..."
    )
    print("Querying list of packages...")
    result = requests.get(
        "https://gitlab.com/api/v4/projects/43746775/packages",
        headers={f"{token_type}-TOKEN": gitlab_token},
    )
    print(f"Request status code {result.status_code}")
    if result.status_code != 200:
        print("No packages found!")
    else:
        for pkg in result.json():
            if pkg.get("name", None) == package_name.replace("_", "-"):
                print(f"Delete package with id {pkg['id']}...")
                requests.delete(
                    f"https://gitlab.com/api/v4/projects/43746775/packages/{pkg['id']}",
                    headers={f"{token_type}-TOKEN": gitlab_token},
                )
                print(f"Package with id {pkg['id']} deleted!")
            else:
                print(
                    f"[bold red]UNEXCPECTED EVENT: Found package with name {pkg.get('name', None)}, "
                    "which is not under consideration.[/bold red]"
                )
    print(f"Finihes deleting package named [bold red]{package_name}[/bold red]!")


if __name__ == "__main__":
    typer.run(delete_package)
